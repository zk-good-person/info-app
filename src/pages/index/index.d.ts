
declare type currentObjType = {
    label:string,
    type:string,
    options?:string[];
}

declare type dataType = currentObjType[]