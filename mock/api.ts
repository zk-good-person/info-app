
export default {
    'GET /api/forms/11': {
        "data":[
            {
                "type":"text",
                "label":"姓名"
            },
            {
                "type":"tel",
                "label":"手机号"
            },
            {
                "type":"email",
                "label":"邮箱"
            },
            {
                "type":"cor",
                "label":"公司"
            },
            {
                "type":"singleSelect",
                "label":"行业",
                "options":["工业4.0","航空航天"]
            },
            {
                "type":"position",
                "label":"职位",
                "options":["工程师","运输员"]
            }
        ]
    },
}